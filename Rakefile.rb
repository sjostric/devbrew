require 'rake'
require 'tmpdir'
require 'fileutils'

#BIN = "#{ENV['HOME']}/app/bin"
#APP = "#{ENV['HOME']}/app"
#DEV = "#{ENV['HOME']}/dev"

BIN = "#{Dir::tmpdir}/app/bin"
APP = "#{Dir::tmpdir}/app"
DEV = "#{Dir::tmpdir}/dev"

apps = {
  'spring-tool-suite_3.7.3' => {
  url: 'http://download.springsource.com/release/TOOLS/update/3.7.3.RELEASE/e4.6/springsource-tool-suite-3.7.3.RELEASE-e4.6-updatesite.zip',
  type: 'zip',

  },
  'maven_3.3.9' => {url: 'http://apache.mirrors.spacedump.net/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.zip', type: 'zip', dir: 'apache-maven-3.3.9'},
}

task :init do |t|

  FileUtils::mkdir_p BIN
  FileUtils::mkdir_p APP
  FileUtils::mkdir_p DEV

  puts "Created directories: "
  puts BIN
  puts APP
  puts DEV
end

task :install, [:app, :ver] do |t, args|
  begin
    puts "Installing: #{args}"
    key = "#{args[:app]}_#{args[:ver]}"
    app_data = apps[key]
    app_name = key.split('_')[0]
    app_version = key.split('_')[1]
    if not app_data
      raise Exception.new("App data not found")
    end
    
    
    tmp = "#{Dir::tmpdir}/brake"
    sh('wget', app_data[:url], '-O', tmp)
      
    if app_data[:type] == 'zip'
      sh('unzip', tmp, '-d', APP)    
    else
      raise Exception.new("Unknown type: #{app_data[:zip]}")
    end
    
    if app_data[:dir]
      FileUtils::mkdir_p "#{APP}/#{app_name}"
      FileUtils::mv "#{APP}/#{app_data[:dir]}", "#{APP}/#{app_name}/#{app_version}"
      puts "Installed to #{APP}/#{app_name}/#{app_version}"
    else
      puts "Installed to #{APP}/#{app_data[:dir]}"
    end
  ensure
    FileUtils::rm tmp
  end

end

task :apt_install, [:app] do |t, args|
  sh('sudo', 'apt-get', 'install', args[:app])
end


