class Jdk8 < Formula

	url "http://download.oracle.com/otn-pub/java/jdk/8u73-b02/jdk-8u73-linux-x64.tar.gz"

 	def install
		prefix.install Dir["*"] 
		#bin.install_symlink prefix/"sts-3.7.3.RELEASE/STS"
  	end
end
