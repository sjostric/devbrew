class SpringToolSuite < Formula

	url "http://dist.springsource.com/release/STS/3.7.3.RELEASE/dist/e4.5/spring-tool-suite-3.7.3.RELEASE-e4.5.2-linux-gtk-x86_64.tar.gz"

 	def install
		prefix.install Dir["*"] 
		bin.install_symlink prefix/"sts-3.7.3.RELEASE/STS"
  	end
end
