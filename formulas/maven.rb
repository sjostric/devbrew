class Maven < Formula

	url "http://apache.mirrors.spacedump.net/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz"

 	def install
		prefix.install Dir["*"]
		# needed fix since brew creates lib/lib because of some magic
		system "mv #{prefix}/lib/lib/* #{prefix}/lib"
  	end

end
